<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Billplz extends Model
{
   protected $fillable = [
      'payment_id', 'reference_no', 'bill_id', 'reference_1', 'reference_1', 'paid', 'json_response'
   ];

    public function scopeCheckXSignature($query, $request) {
        $data = 
           [
              "amount"        => $request->amount,
              "collection_id" => $request->collection_id,
              "due_at"        => $request->due_at,
              "email"         => $request->email,
              "id"            => $request->id,
              "mobile"        => $request->mobile,
              "name"          => $request->name,
              "paid_amount"   => $request->paid_amount,
              "paid_at"       => $request->paid_at,
              "paid"          => $request->paid,
              "state"         => $request->state,
              "url"           => $request->url,
           ];
 
        foreach ($data as $key => $value) {
           $new_data[] = $key.$value;
        }
 
        $new_data = implode('|', $new_data);
 
        $x_signature = env('BILLPLZ_X_SIGNATURE');
        $query = hash_hmac('sha256', $new_data, $x_signature);
        
        return $query;
    }

   public function scopeCreateBillAPI($query, $request) {
      $amount = ($request->amount * 100);
 
      $billplz_host          = env('BILLPLZ_HOST');
      $billplz_api_key       = env('BILLPLZ_API_KEY');
      $billplz_x_signature   = env('BILLPLZ_X_SIGNATURE');
      $billplz_collection_id = env('BILLPLZ_COLLECTION_ID');
 
      try {
         $client = new Client();
         $response = $client->post($billplz_host.'v3/bills', [
            'auth' => [$billplz_api_key, 'password'],
            'form_params' => [
               'collection_id'     => $billplz_collection_id,
               'email'             => $request->email,
               'mobile'            => $request->phone,
               'name'              => $request->name,
               'amount'            => $amount,
               'description'       => 'Reference No: '.$request->reference_no,
               'reference_2_label' => 'Payment ID',
               'reference_2'       => $request->payment_id,
               'callback_url'      => env('BILLPLZ_CALLBACK_URL'), #our side
               'redirect_url'      => isset($request->redirect_url) ? $request->redirect_url : null, #mobile side
            ]
         ]);
  
         return json_decode($response->getBody());
 
      } catch (GuzzleException $exception) {
         $decoded_response = json_decode($exception->getResponse()->getBody(true));
 
         $response = [
         //   'error' => 'Something went wrong',
            'error' => $decoded_response
         ];

         // return response()->json($response, 400);
         return $response;
      }
   }

   public function scopeGetBillAPI($query, $id) {
      try {
         $client = new Client();

         $response = $client->get(env('BILLPLZ_HOST').'v3/bills/'.$id, 
            [
               'auth' => [env('BILLPLZ_API_KEY'), 'password']
            ]
         );

         return json_decode($response->getBody());

      } catch (GuzzleException $exception) {
         $decoded_response = json_decode($exception->getResponse()->getBody(true));

         $response = [
         'error' => 'Something went wrong'
         ];

         return json_decode($exception->getResponse()->getBody(true));
         // return json_decode($exception->getBody());
      }
   }

   public function scopeGetBill($query, $bill_id) {
      $query = $this->select('bill_id', 'payment_id')->where('bill_id', $bill_id)->first();
      return $query; 
   }
}
