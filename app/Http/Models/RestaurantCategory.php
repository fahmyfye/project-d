<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\RestaurantStaff;

class RestaurantCategory extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'name', 'image', 'sorting', 'is_active',
   ];

   public function scopeName($query, $id)
   {
   	$query = $this->where('id', $id)->value('name');
   	return $query;
   }

   public function subcategory()
   {
      return $this->hasMany('App\Http\Models\RestaurantSubCategory', 'category_id', 'id')->select('id', 'category_id', 'name', 'image', 'sorting')->where('is_active', 1);
   }
}
