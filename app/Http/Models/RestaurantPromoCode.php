<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantPromoCode extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'restaurant_id', 'name', 'code', 'type', 'value', 'allocation', 'is_limited', 'multiple_usage', 'is_active', 'created_by', 'updated_by'
    ];

    public function scopeGetAmount($query, $code, $amount)
    {
        $discount = $this->select('type', 'value')->where('code', $code)->first();
        if ($discount && !empty($discount)) {
            if ($discount->type == 'percentage') {
                $total = $amount * ($discount->value / 100);

            } elseif ($discount->type == 'amount') {
                $total = $discount->value;
            }
        } else {
            return $query = 'invalid';
        }

        return $query = $total;
    }
}
