<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SalesTransaction extends Model
{
   	protected $fillable = [
      	'payment_id', 'reference_no', 'customer_id', 'customer_ip', 'amount', 'currency', 'gateway', 'payment_type', 'raw_json', 'status', 'initialized',
   	];
}
