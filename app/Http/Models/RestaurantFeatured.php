<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantFeatured extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'ops_start', 'ops_end', 'ops_date', 'is_active',
   ];

   public function restaurant()
   {
      return $this->hasOne('App\Http\Models\Restaurant', 'id', 'restaurant_id')->select('id', 'name', 'cover as image');
   }
}
