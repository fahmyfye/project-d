<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantSalesDetails extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'reference_no', 'restaurant_id', 'customer_id', 'menu_id', 'menu_name', 'menu_entity_id', 'menu_entity_name', 'quantity', 'price', 'total', 'remarks',
   ];
}
