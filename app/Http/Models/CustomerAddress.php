<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerAddress extends Model
{
   use SoftDeletes;

   protected $fillable = [
      'customer_id', 'name', 'phone', 'address1', 'address2', 'postcode', 'city', 'state', 'country', 'default', 'active',
   ];

   public function scopeGetAddress($query, $id)
   {
   	$query = $this->select('id', 'name', 'phone', 'address1', 'address2', 'postcode', 'city', 'state', 'country')
   		->where('id', $id)
   		->first();

   	return $query;
   }
}
