<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantMenu extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'name', 'price', 'description', 'ingredients', 'category', 'image', 'is_promotion', 'promotion_price', 'sorting', 'is_active',
   ];

    public function scopeCheckAvailbility($query, $cart)
    {
		foreach($cart as $value) {
			$query = $this->where('id', $value->menu_item_id)->value('is_active');
			if($query == 0) {
				return $query;
			} else {
				$query = 1;
			}
		}

		return $query;
    }

   	public function scopeGetAmount($query, $cart)
   	{
   		$total = 0;
		foreach($cart as $value) {
			$price           = $this->where('id', $value->menu_item_id)->value('price');
			$subtotal        = round(($price * $value->qty), 2);
			$value->subtotal = $subtotal;
			$total           += $subtotal;
		}

		return $query = ['cart' => $cart, 'amount' => $total];
   	}
}