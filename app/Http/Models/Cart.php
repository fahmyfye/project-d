<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
   use SoftDeletes;

   protected $fillable = [
      'customer_id', 'menu_item_id', 'qty',
   ];
}
