<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
   protected $fillable = [
      'customer_id', 'api_token', 'email', 'module', 'otp',
   ];

   public function scopeGenerate()
   {
      // 5 digit otp
   	$otp = null; 
   	for($x = 0; $x < 5; $x++) {
         $otp .= rand(0, 9);
      }

      return $otp;
   }
}
