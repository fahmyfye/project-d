<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\RestaurantStaff;

class RestaurantBundle extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'menus', 'name', 'price', 'description', 'category', 'image', 'is_promotion', 'promotion_price',
      'time_limited', 'time_start', 'time_end', 'sorting', 'is_active',
   ];
}