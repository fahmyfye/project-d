<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantSalesOrder extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'reference_no', 'restaurant_id', 'type', 'address_id', 'customer_id', 'email', 'amount', 'discount_code', 'discount_amount', 'delivery_amount',
      'total_amount', 'order_status', 'delivery_status',
   ];
}
