<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantPettyCash extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'cash', 'additional', 'closing',
   ];

   protected $attribute = [
   	'additional' => '{
   		"myr100": "0",
   		"myr50": "0",
   		"myr20": "0",
   		"myr10": "0",
   		"myr5": "0",
   		"myr1": "0",
   		"myr050": "0",
   		"myr020": "0",
   		"myr010": "0",
   		"myr005": "0"
   	}',

   	'closing' => '{
   		"myr100": "0",
   		"myr50": "0",
   		"myr20": "0",
   		"myr10": "0",
   		"myr5": "0",
   		"myr1": "0",
   		"myr050": "0",
   		"myr020": "0",
   		"myr010": "0",
   		"myr005": "0"
   	}',
   ];
}
