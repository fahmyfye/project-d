<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'name', 'is_active'
   ];
}
