<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
   use SoftDeletes;

	/**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
      'name', 'email', 'device', 'password', 'phone', 'latitude', 'longitude', 'city', 'state', 'avatar', 'api_token', 'social', 'social_id',
   ];

   /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
   protected $hidden = [
      'password', 'remember_token',
   ];

   /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
   protected $casts = [
      'email_verified_at' => 'datetime',
   ];

   public function scopeGetInfo($query, $api_token)
   {
      $query = $this->select('id', 'name', 'email', 'device', 'phone', 'latitude', 'longitude', 'city', 'state', 'avatar', 'api_token', 'firebase_token', 'social', 'social_id')
         ->where('api_token', $api_token)
         ->first();

      return $query;
   }

   public function scopeGetId($query, $api_token)
   {
      $query = $this->where('api_token', $api_token)->value('id');

      if($query) {
        return $query;

      } else {
        return $query = 0;

      }
   }
}
