<?php

namespace App\Http\Models;

use App\Http\Models\RestaurantSalesOrder;
use App\Http\Models\RestaurantOrderStatus;
use GuzzleHttp\Client;

use Illuminate\Database\Eloquent\Model;

class eGHL extends Model
{
    public function scopePaymentRequest($query, $trx)
    {
        $password = 'sit12345';
        $service_id = 'sit';
        $orders = RestaurantSalesOrder::select('total_amount', 'order_status')->where('reference_no', $trx->reference_no)->first();
        $txnstatus = RestaurantOrderStatus::where('slug', $orders->order_status)->value('id');
        $currency = 'MYR';
        $merc_name = 'Project-D';
        $merc_return = 'http://127.0.0.1:8000/api/v1/transaction/eghl/callback?eghl=123';
        $merc_callback = 'http://127.0.0.1:8000/api/v1/transaction/eghl/callback?eghl=123';
        $merc_terms = 'http://127.0.0.1:8000/api/v1/transaction/eghl/callback';
        $page_timeout = '7800';

        // Password + ServiceID + PaymentID +
        // MerchantReturnURL + MerchantApprovalURL + MerchantUnApprovalURL + MerchantCallBackURL +
        // Amount + CurrencyCode + CustIP + PageTimeout + CardNo + Token

        // Hash Key = Password + TxnID + ServiceID + PaymentID + TxnStatus + Amount + CurrencyCode + AuthCode
        // Password + ServiceID + PaymentID + MerchantReturnURL + Amount + CurrencyCode + CustIP + PageTimeout

        $hashRawValues = [
            $password,
            $service_id,
            $trx->payment_id,
            $merc_return,
            $merc_callback,
            $orders->total_amount,
            $currency,
            $trx->customer_ip,
            $page_timeout
        ];

        $my_hash = hash(
            'sha256',
            $password .
                $service_id .
                $trx->payment_id .
                $merc_return .
                // $merc_callback .
                $orders->total_amount .
                $currency .
                $trx->customer_ip .
                $page_timeout
        );

        // echo $my_hash;
        // exit;

        // $my_hash = hash('sha256', $my_string);
        // $my_hash = sha256($my_string);

        try {
            $client = new Client();
            $response = $client->request(
                'POST',
                'https://test2pay.ghl.com/IPGSG/Payment.aspx',
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Accept' => 'application/json;charset=UTF-8'
                    ],
                    'form_params' => [
                        'TransactionType' => 'SALE',
                        'PymtMethod' => 'ANY',
                        'ServiceID' => $service_id,
                        'PaymentID' => $trx->payment_id,
                        'OrderNumber' => $trx->reference_no,
                        'PaymentDesc' => 'Reference No.: ' . $trx->reference_no,
                        'MerchantName' => $merc_name,
                        'MerchantReturnURL' => $merc_return,
                        // 'MerchantCallbackURL' => $merc_callback,
                        'Amount' => $orders->total_amount,
                        'CurrencyCode' => $currency,
                        'CustIP' => $trx->customer_ip,
                        'CustName' => $trx->customer->name,
                        'CustEmail' => $trx->customer->email,
                        'CustPhone' => $trx->customer->phone,
                        'HashValue' => $my_hash,
                        'MerchantTermsURL' => $merc_terms,
                        'LanguageCode' => 'en',
                        'PageTimeout' => $page_timeout
                    ]
                ]
            );

            // $decoded_response = $response->getBody();
            // $decoded_response = json_decode($response->getBody(true));
            parse_str($response->getBody(), $decoded_response);
            // echo $response->getBody();

            // $decoded_response = [
            //     '_status' => 'OK',
            //     'payment_id' => $trx->payment_id,
            //     'order_id' => $trx->reference_no,
            //     'amount' => $orders->total_amount,
            // ];

        } catch (GuzzleException $exception) {
            $decoded_response = json_decode($exception->getResponse()->getBody(true));
        }

        // For sample purposes until payment gateway fix
        $decoded_response = 1;
        return $decoded_response;
    }
}

// <input type="hidden" name="TransactionType" value="SALE">
// <input type="hidden" name="PymtMethod" value="ANY">
// <input type="hidden" name="ServiceID" value="A00">
// <input type="hidden" name="PaymentID" value="ABCDEFGH130820142128">
// <input type="hidden" name="OrderNumber" value="IJKLMN">
// <input type="hidden" name="PaymentDesc" value="Booking No: IJKLMN, Sector: KUL-BKI, First Flight Date: 26 Sep 2012">
// <input type="hidden" name="MerchantName" value="Merchant A">
// <input type="hidden" name="MerchantReturnURL" value="https://merchA.merchdomain.com/pymtresp.aspx">
// <input type="hidden" name="MerchantCallbackURL" value="https://merchA.merchdomain.com/pymtrespcallback.aspx">
// <input type="hidden" name="Amount" value="228.00">
// <input type="hidden" name="CurrencyCode" value="MYR">
// <input type="hidden" name="CustIP" value="192.168.2.35">
// <input type="hidden" name="CustName" value="Jason">
// <input type="hidden" name="CustEmail" value="Jasonabc@gmail.com">
// <input type="hidden" name="CustPhone" value="60121235678">
// <input type="hidden" name="HashValue" value="hash value generated">
// <input type="hidden" name="MerchantTermsURL" value="http://merchA.merchdomain.com/terms.html">
// <input type="hidden" name="LanguageCode" value="en">
// <input type="hidden" name="PageTimeout" value="780">
