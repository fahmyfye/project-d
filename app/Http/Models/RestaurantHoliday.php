<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\Restaurant;
use Carbon\Carbon;

class RestaurantHoliday extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'ops_start', 'ops_end', 'ops_date', 'is_active',
   ];

   public function scopeCronStart()
   {
   	$date = Carbon::now()->format('Y-m-d');
      $time = Carbon::now()->format('H:i:s');
      $holiday = $this->where('ops_date', $date)->where('ops_start', '<=', $time)->where('is_closed', false)->where('i_active', true)->get();
      if($holiday) {
      	foreach($holiday as $value) {
      		$restaurant = Restaurant::where('id', $value->restaurant_id)->update(['is_closed' => false]);
      	}
      }
   }
}
