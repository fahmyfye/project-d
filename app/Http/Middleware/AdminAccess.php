<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            if(!empty(Auth::user()->role))
            {
                if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->route('error');
                }

            }
            else
            {
                return redirect()->route('login');
            }
        }
        else
        {
            return redirect()->route('login');
        }
    }
}
