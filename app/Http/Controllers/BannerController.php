<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Banner;
use Illuminate\Support\Facades\Storage;
use Str;

class BannerController extends Controller
{
    public function index()
    {
        $data = Banner::where('is_active', true)->get();
        return view('app.banner.index', compact('data'));
    }

    public function create()
    {
        return view('app.banner.create');
    }

    public function store(Request $request)
    {
        // Validate
        $request->validate([
            'name'    => 'required|string|max:255',
            'image'   => 'image|max:2048|mimes:png,jpeg',
            'sorting' => 'numeric',
        ]);

        if ($request->file('image')) {
            $files = $request->file('image');

            // Get Random String
            $str = Str::random(5);

            // Get File Extension
            $ext = $request->file('image')->getClientOriginalExtension();

            // Set Name
            $name = $str . '_banner.' . $ext;

            // Set Path
            $link = '/images/banner/' . $name;
            $path = '/storage' . $link; // (for local storage add 'storage/')

            $saveimg = Storage::disk('local')->put($link, file_get_contents($files), 'public');

            if ($saveimg) {
                $data = Banner::create([
                    'name'          => ucwords($request->name),
                    'image'         => $path,
                    'sorting'       => $request->sorting,
                ]);
            } else {
                session()->flash('message', 'Fail to Save Banner Image');
                session()->flash('alert-class', 'alert-warning');
                return redirect()->back();
            }
        } else {
            $data = Banner::create([
                'name'          => ucwords($request->name),
                'image'         => null,
                'sorting'       => $request->sorting,
            ]);
        }

        // Return Response
        if ($data) {
            session()->flash('message', 'Banner Saved');
            session()->flash('alert-class', 'alert-success');
            return redirect()->route('settings.banner.index');
        } else {
            session()->flash('message', 'Fail to Save Banner');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $data = Banner::where('id', $id)->first();
        return view('app.banner.edit', compact('data'));
    }

    public function update(Request $request, $restaurantid)
    {
        // Validate
        $request->validate([
            'name'         => 'required|string|max:255',
            'image'        => 'image|max:2048|mimes:png,jpeg',
            'sorting'      => 'numeric',
        ]);

        if ($request->file('image')) {
            $files = $request->file('image');

            // Get Random String
            $str = Str::random(5);

            // Get File Extension
            $ext = $request->file('image')->getClientOriginalExtension();

            // Set Name
            $name = $str . '_cover.' . $ext;

            // Set Path
            $link = '/images/banner/' . $name;
            $path = '/storage' . $link; // (for local storage add 'storage/')

            $saveimg = Storage::disk('local')->put($link, file_get_contents($files), 'public');
        } else {
            $path = Banner::where('id', $request->id)->value('image');
        }

        $data = Banner::where('id', $request->id)->update([
            'name'      => $request->name,
            'image'     => $path,
            'sorting'   => $request->sorting,
            'is_active' => isset($request->is_active) ? 1 : 0,
        ]);

        // Return Response
        if ($data) {
            session()->flash('message', 'Banner Updated');
            session()->flash('alert-class', 'alert-success');
            return redirect()->route('settings.banner.index');
        } else {
            session()->flash('message', 'Fail to Update Banner');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
        }
    }

    public function delete(Request $request)
    {
        $data = Banner::where('id', $request->id)->delete();

        // Return Response
        if ($data) {
            session()->flash('message', 'Banner Deleted');
            session()->flash('alert-class', 'alert-success');
            return redirect()->back();
        } else {
            session()->flash('message', 'Fail to Delete Banner');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
        }
    }
}
