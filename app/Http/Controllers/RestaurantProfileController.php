<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Restaurant;

class RestaurantProfileController extends Controller
{
	const DAYS = 
	[
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
		'Sunday'
	];

   public function index($restaurantid) 
   {
		$data = Restaurant::where('id', $restaurantid)->first();
		$days = self::DAYS;
   	return view('restaurant.profile.view', compact('restaurantid', 'data', 'days'));
   }
}
