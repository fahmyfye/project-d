<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Role;
use App\Http\Models\Restaurant;
use App\Http\Models\RestaurantStaff;
use Auth;
use Hash;

class RestaurantStaffController extends Controller
{
	// const ROLES = 
	// [
 //      [
 //         'name' => 'Super Admin',
 //         'role' => 'superadmin'
 //      ],
	// 	[
	// 		'name' => 'Admin',
	// 		'role' => 'admin'
	// 	],
	// 	[
	// 		'name' => 'Staff',
	// 		'role' => 'staff'
	// 	],
	// ];

   public function index($restaurantid)
   {
		$data = RestaurantStaff::where('restaurant_id', $restaurantid)
         ->where('is_active', 1)
			->with('users')
			->orderByRaw('CASE WHEN role = "superadmin" THEN 1 WHEN role = "admin" THEN 2 ELSE 3 END')
			->get();

      $archived = RestaurantStaff::where('restaurant_id', $restaurantid)
         ->where('is_active', 0)
         ->with('users')
         ->orderByRaw('CASE WHEN role = "superadmin" THEN 1 WHEN role = "admin" THEN 2 ELSE 3 END')
         ->get();

      $restaurant = Restaurant::select('id', 'name')->where('id', $restaurantid)->first();

      return view('restaurant.staff.index', compact('restaurantid', 'data', 'archived', 'restaurant'));
   }

   // Update Staff Status
   public function status(Request $request, $restaurantid)
   {
      $auth = auth()->user()->role;

      if($auth == 'superadmin' || $auth == 'admin') {
         $data = RestaurantStaff::updateStatus($restaurantid, $request);

      } else {
         $restaurant_auth = RestaurantStaff::restaurantRole($restaurantid);
         if($restaurant_auth == 'superadmin' || $restaurant_auth == 'admin') {
            $data = RestaurantStaff::updateStatus($restaurantid, $request);

         } else {
            session()->flash('message', 'You are not authorize to make an update');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
         }
      }
      
   	// Return Response
   	if($data) {
   		session()->flash('message', 'Staff Status Updated');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	} else {
   		session()->flash('message', 'Fail to Update Staff Status');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   // Retrieve In-Active Staff
   public function archived($restaurantid)
   {
		$data    = RestaurantStaff::where('restaurant_id', $restaurantid)->where('is_active', 0)->with('users')->get();
      $restaurant = Restaurant::select('id', 'name')->where('id', $restaurantid)->first();
		$role    = RestaurantStaff::restaurantRole($restaurantid);
   	return view('restaurant.staff.archived', compact('restaurantid', 'data', 'restaurant', 'role'));
   }

   // Edit Profile
   public function profile()
   {
   	$data = User::where('id', auth()->id())->with('restaurantstaff')->first();
   	return view('restaurant.staff.profile', compact('data'));
   }

   // System Admin Assigned Staff to Restaurant
   public function staffIndex($restaurantid)
   {
      $data = RestaurantStaff::where('restaurant_id', $restaurantid)
         ->with('users')
         ->orderBy('is_active', 'desc')
         ->orderByRaw('CASE WHEN role = "superadmin" THEN 1 WHEN role = "admin" THEN 2 ELSE 3 END')
         ->get();

      $restaurant = Restaurant::select('id', 'name')->where('id', $restaurantid)->first();
      $filter     = RestaurantStaff::select('user_id')->where('restaurant_id', $restaurantid)->get();
      $user       = User::select('id', 'name')->whereNotIn('id', $filter)->where('role', 'restaurant')->where('is_active', 1)->get();

      return view('user.assignedUser', compact('restaurantid', 'data', 'restaurant', 'user'));
   }

   public function staffAssigned(Request $request, $restaurantid)
   {
      // Validate
      $request->validate([
         'user_id' => 'required',
         'role'    => 'required',
      ]);

      $data = RestaurantStaff::create([
         'restaurant_id' => $restaurantid,
         'user_id'       => $request->user_id,
         'role'          => $request->role,
      ]);
      
      // Return Response
      if($data) {
         session()->flash('message', 'Staff Assigned Successfully');
         session()->flash('alert-class', 'alert-success');
         return redirect()->back();

      } else {
         session()->flash('message', 'Fail to Assigned Staff');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }
}
