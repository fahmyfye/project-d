<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\RestaurantMenu;
use App\Http\Models\RestaurantSalesOrder;
use App\Http\Models\RestaurantSalesDetails;
use App\Http\Models\Customer;
use App\Http\Models\CustomerAddress;
use App\Http\Models\RestaurantPromoCode;
use App\Http\Models\Cart;
use App\Http\Models\SalesTransaction;
use Validator;
use Str;
use Carbon\Carbon;

class SalesController extends Controller
{
	// List All Orders
	public function index(Request $request)
	{
		$customer_id = Customer::getId($request->header('Authorization'));
		$sales       = RestaurantSalesOrder::select('id', 'reference_no', 'type', 'order_status as orderStatus', 'amount as subtotal', 'discount_code', 'discount_amount', 'total_amount as total')
			->where('customer_id', $customer_id)
			->orderBy('id', 'desc')
			->get();

		if($sales && count($sales) > 0) {
			$data = $sales->map(function ($value, $key) {
				$value['subtotal']    = number_format($value['subtotal'], 2);
				$value['total']       = number_format($value['total'], 2);
			    $value['total_items'] = RestaurantSalesDetails::where('reference_no', $value['reference_no'])->count();
			    return $value;
			});
			return response()->json($data, 200);
		} else {
			return response()->json(['_errors' => ['No orders']], 422);
		}

	}

	// Create order
   	public function create(Request $request)
   	{
		$customer_id = Customer::getId($request->header('Authorization'));
		$cart        = Cart::select('menu_item_id', 'qty')->where('customer_id', $customer_id)->get();

    	if($cart && count($cart) > 0) {
    		$check  = RestaurantMenu::checkAvailbility($cart);

			if($check == 1) {
				$reference_no    = Carbon::now()->format('YmdHis').strtoupper(Str::random(4));
				$getAmount       = RestaurantMenu::getAmount($cart);
				$discount_amount = 0;
				$delivery_amount = 0;

				// If customer use discount code
				if(isset($request->discount_code)) {
					$discount_amount = RestaurantPromoCode::getAmount($request->discount_code, $getAmount['amount']);
					// if discount is invalid
					if($discount_amount == 'invalid') {
						return response()->json(['_errors' => ['Invalid promo code']], 422);
					}
				}

				// Calculate Total
				$total_amount = ($getAmount['amount'] - $discount_amount) + $delivery_amount;

				// Save in SalesOrder
				$createOrder = RestaurantSalesOrder::create([
					'reference_no'    => $reference_no,
					'restaurant_id'   => (int) $request->restaurant_id,
					'type'            => $request->type,
					'address_id'      => isset($request->address_id) ? (int) $request->address_id : null,
					'customer_id'     => $customer_id,
					'email'           => $request->email,
					'amount'          => $getAmount['amount'],
					'discount_code'   => isset($request->discount_code) ? $request->discount_code : null,
					'discount_amount' => isset($discount_amount) ? $discount_amount : 0,
					'delivery_amount' => isset($request->delivery_amount) ? $request->delivery_amount : 0,
					'total_amount'    => $total_amount,
				]);

				$order = RestaurantSalesOrder::select('reference_no', 'restaurant_id', 'type', 'address_id', 'email', 'discount_code', 'discount_amount', 'total_amount')
					->where('reference_no', $reference_no)->first();

				// Save in OrderDetails
				// $details = [];
				foreach($cart as $value) {
					$menu = RestaurantMenu::select('id', 'name', 'price')->where('id', $value->menu_item_id)->first();
					$total = $value->qty * $menu->price;

					$createSales = RestaurantSalesDetails::create([
						'reference_no'  => $reference_no,
						'restaurant_id' => (int) $request->restaurant_id,
						'customer_id'   => $customer_id,
						'menu_id'       => $value->menu_item_id,
						'menu_name'     => $menu->name,
						'quantity'      => $value->qty,
						'price'         => $menu->price,
						'total'         => $total,
					]);

					$sales = RestaurantSalesDetails::select('menu_name', 'quantity', 'price', 'total')->where('id', $createSales->id)->first();

					// array_push($details, $sales);
				}

				// $debug = [
				// 	'order' => $order,
				// 	'details' => $details
				// ];

				$data = [
					'_status' => 'OK',
					'order_id' => $reference_no,
				];

				if($createOrder) {
					return response()->json($data, 200);

				} else {
					return response()->json(['_errors' => ['Fail to create order']], 422);
				}
			}
		} else {
			return response()->json(['_errors' => ['Fail to create order. Cart is empty']], 422);
		}

   	}

   	public function details(Request $request)
   	{
   		$orders  = RestaurantSalesOrder::where('reference_no', $request->reference_no)->first();
   		$menu_id = RestaurantSalesDetails::where('reference_no', $request->reference_no)->pluck('menu_id');
   		$details = RestaurantMenu::select('id', 'name', 'image', 'price', 'description')->whereIn('id', $menu_id)->get();
   		$address = CustomerAddress::select('id', 'name', 'phone', 'address1', 'address2', 'postcode', 'city', 'state', 'country')
   			->where('id', $orders->address_id)
   			->first();

   		if($orders->order_status == 'completed') {
   			$sales = SalesTransaction::where('reference_no', $request->reference_no)->first();
   			$payment =
	   			[
	   				'payment_type' => $sales->payment_type,
	   				'payment_status' => $sales->status
	   			];

   		} else {
   			$payment =
	   			[
	   				'payment_type' => null,
	   				'payment_status' => 'Pending'
	   			];
   		}

   		$promo = null;
   		if($orders->discount_code && $orders->discount_code != null) {
   			$promo = RestaurantPromoCode::select('id', 'code', 'value', 'type')->where('code', $orders->discount_code)->first();
   			if($promo->type == 'amount') {
   				$promo->message = 'MYR'.$promo->value.' coupon applied';

   			} elseif($promo->type == 'percentage') {
   				$promo->message = $promo->value.'% coupon applied';
   			}

   			// unset($promo->type);
   		}

   		$data = [
   			'id'               => $orders->id,
   			'reference_no'     => $orders->reference_no,
   			'orderStatus'      => $orders->order_status,
   			'type'             => $orders->type,
   			'subtotal'         => number_format($orders->amount, 2),
   			'discount_amount'  => '( '.number_format($orders->discount_amount, 2).' )',
   			'total'            => number_format($orders->total_amount, 2),
   			'total_products'   => count($details),
   			'coupon_code'      => $promo,
   			'shipping_details' => $address,
   			'products'         => $details,
   			'payment'		   => $payment,
   		];

   		return response()->json($data, 200);
   	}
}
