<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\State;

class StateController extends Controller
{
   public function index()
   {
   	$data = State::where('is_active', true)->get();
   	if($data) {
   		return response()->json($data, 200);

   	} else {
   		return response()->json(['_errors' => ['Server error']], 400);

   	}
   }
}
