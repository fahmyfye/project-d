<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\RestaurantCategory;
use URL;

class CategoryController extends Controller
{
   public function index($restaurantid)
   {
   	$restaurant = RestaurantCategory::select('id', 'name', 'image')
   		->where('restaurant_id', $restaurantid)
   		->where('is_active', true)
   		->orderBy('sorting')
   		->get();

   	if($restaurant) {
   	   $url = URL::to('/');
   	   $data = $restaurant->map(function ($value, $key) use ($url) {
   	      if(substr($value['image'], 0, 4) !== 'http'){
	   		   $value['image'] = $url.$value['image'];
	   		} else {
	   			$value['image'] = $value['image'];
	   		}
	   		return $value;
   	   });
   	}

   	return response()->json($data, 200);
   }
}
