<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\RestaurantTable;

class RestaurantTableController extends Controller
{
   	public function index($restaurantid, $tableno)
   	{
   		$data = 
	   		[
	   			'restaurantid' => $restaurantid,
	   			'tableno' => $tableno
	   		];

   		// echo url('/');
	   		// api/v1/
		
		return response()->json($data, 200);
   	}
}
