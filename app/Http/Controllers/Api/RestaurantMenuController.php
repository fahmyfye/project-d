<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\RestaurantMenu;
use URL;

class RestaurantMenuController extends Controller
{
    public function index($restaurantid) {
        $menu = RestaurantMenu::select('id', 'restaurant_id', 'name', 'price', 'description', 'category', 'image', 'is_promotion', 'promotion_price')
			->where('is_active', true)
			->where('restaurant_id', $restaurantid)
			->orderBy('sorting')
			->get();

        $url = URL::to('/');
        $data = $menu->map(function ($value, $key) use ($url) {
            if (substr($value['image'], 0, 4) !== 'http') {
                $value['image'] = $url.$value['image'];
            } else {
                $value['image'] = $value['image'];
            }
            return $value;
        });

        if ($data) {
            return response()->json($data, 200);
        } else {
            return response()->json(['_errors' => ['No menu available']], 422);
        }
	}
	
	public function item($restaurantid, $categoryid) {
        $menu = RestaurantMenu::select('id', 'restaurant_id', 'name', 'price', 'description', 'category', 'image', 'is_promotion', 'promotion_price')
			->where('is_active', true)
			->where('restaurant_id', $restaurantid)
			->where('category', $categoryid)
			->orderBy('sorting')
			->get();

        $url = URL::to('/');
        $data = $menu->map(function ($value, $key) use ($url) {
            if (substr($value['image'], 0, 4) !== 'http') {
                $value['image'] = $url.$value['image'];
            } else {
                $value['image'] = $value['image'];
            }
            return $value;
        });

        if ($data) {
            return response()->json($data, 200);
        } else {
            return response()->json(['_errors' => ['No menu available']], 422);
        }
	}
}
