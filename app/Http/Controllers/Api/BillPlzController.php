<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Billplz;
use App\Http\Models\RestaurantSalesOrder;
use App\Http\Models\SalesTransaction;

class BillPlzController extends Controller
{
    public function create (Request $request) {
        // Create Bill
        if(isset($request->email) && isset($request->phone) && isset($request->name) && isset($request->amount) && isset($request->payment_id)) {
            $response = Billplz::CreateBillAPI($request);

            if($response) {
                if(!isset($response->error)) {
                    $create = Billplz::create([
                        'bill_id' => $response->id,
                        'reference_no' => $request->reference_no,
                        'reference_1' => $response->reference_1,
                        'payment_id' => $response->reference_2,
                        'json_response' => json_encode($response)
                    ]);

                    // Return Success Response
                    return response()->json($response, 200);

                } else {
                    // Return Failed Response With Errors
                    return response()->json($response, 422);
                }
            }

        } else {
            return response()->json(['error' => 'Missing Parameters'], 422);
        }
    }


    public function callback(Request $request) {
        // Validate X Signature
        $x = Billplz::CheckXSignature($request);
        
        // If Validated
        if($x == $request->x_signature) {
            // Get Bill from DB
            $bill = Billplz::getBill($request->id);
            if(!empty($bill)) {
                if($x->paid == 'true') {
                    $transaction = SalesTransaction::where('payment_id', $bill->payment_id)->first();
                    // If user made payment
                    if($x->state == 'paid') {
                        // If payment is success
                        // Update Sales Order Transaction DB
                        $transaction->initialized = 1;
                        $transaction->status = 'paid';
                        $transaction->raw_json = json_encode($request);
                        $transaction->save();

                        // Update Sales Order DB
                        $sales = RestaurantSalesOrder::find($transaction->order_id);
                        $sales->order_status = 'paid';
                        $sales->save();

                        $data = SalesTransaction::select('payment_id', 'reference_no', 'amount', 'gateway', 'payment_type', 'status')
                            ->where('payment_id', $trx->payment_id)
                            ->where('reference_no', $trx->reference_no)
                            ->first();

                        $data->status = 'completed';
                        $data->save();

                        // Clear cart on success
                        if ($data) {
                            $cart = Cart::where('customer_id', $trx->customer_id)->delete();
                        }

                        // Send Email On Success Payment
                        // if($sales) {
                        //     $details = SalesOrder::where('id', $sales->id)->first();
                        //     $mail = SalesOrderMail::SendingMail($details);
                        // }
                    }
                }

                // Return Response
                return response()->json(['_status' => 'OK'], 200);
            }
        }
    }

    public function query($bill_id) {
        $billplz = Billplz::getBillAPI($bill_id);
        return response()->json($billplz, 200);
    }

    public function redirect() {
        return view('redirect');
    }
}
