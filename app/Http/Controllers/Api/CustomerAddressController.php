<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Models\CustomerAddress;
use App\Http\Models\State;
use Validator;

class CustomerAddressController extends Controller
{
   public function create(Request $request)
   {
   	// Validate Rules
   	$rules = [
         'name'      => 'required|string|max:255',
         'phone'     => 'required',
         'address1'  => 'required|string|max:255',
         'address2'  => 'required|string|max:255',
         'postcode'  => 'required|numeric|digits:5',
         'city'      => 'required|string|max:255',
         'state'     => 'required|string|max:255',
         'country'   => 'required|string|max:255',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

   	if($validator->fails()) {
   		return response()->json(['_errors' => $validator->errors()], 400);

   	} else {
   		$customer_id = Customer::where('api_token', $request->header('Authorization'))->value('id');

   		$data = CustomerAddress::updateOrCreate([
            'id' => $request->address_id
         ],
         [
   			'customer_id' => $customer_id,
            'name'        => ucwords($request->name),
				'phone'       => $request->phone,
				'address1'    => $request->address1,
				'address2'    => $request->address2,
				'address3'    => $request->address3,
				'postcode'    => $request->postcode,
				'city'        => $request->city,
				'state'       => $request->state,
				'country'     => $request->country,
            'default'     => ($request->default) ? $request->default : 0,
   		]);

   		if($data) {
            if($data->default == 1) {
               $remove = CustomerAddress::where('customer_id', $customer_id)->where('active', true)->where('id', '!=', $data->id)
                  ->update([
                  'default' => false,
               ]);
            }

            // $debug = CustomerAddress::getAddress($data->id);

   			return response()->json(['_status' => 'OK'], 200);

   		} else {
   			return response()->json(['_errors' => ['Fail to register']], 400);

   		}
   	}
   }

   public function addressList(Request $request)
   {
      $customer_id = Customer::where('api_token', $request->header('Authorization'))->value('id');
      $data = CustomerAddress::select('id', 'name', 'phone', 'address1', 'address2', 'postcode', 'city', 'state', 'country', 'default')->where('customer_id', $customer_id)->where('active', true)->get();

      if($data) {
         return response()->json($data, 200);

      } else {
         return response()->json(['_errors' => ['No address registered']], 400);

      }
   }

   public function delete(Request $request)
   {
      $data = CustomerAddress::where('id', $request->address_id)->delete();

      if($data) {
         return response()->json(['_status' => 'OK'], 200);

      } else {
         return response()->json(['_errors' => ['Server error']], 400);

      }
   }

   public function state()
   {
      $data = State::where('is_active', true)->pluck('name');
      if($data) {
         return response()->json($data, 200);

      } else {
         return response()->json(['_errors' => ['Server error']], 400);

      }
   }
}
