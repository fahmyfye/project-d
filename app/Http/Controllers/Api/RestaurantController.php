<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Restaurant;
use URL;

class RestaurantController extends Controller
{
   public function index()
   {
   	$restaurant = Restaurant::select('id', 'name', 'cover as image', 'latitude', 'longitude', 
   			'address1', 'address2', 'address3', 'postcode', 'city', 'state', 'country')
   		->where('is_active', true)
   		->get();

      if($restaurant) {
         $url = URL::to('/');
         $data = $restaurant->map(function ($value, $key) use ($url) {
            $value['image'] = ($value['image'] == null) ? null : $url.$value['image'];
            return $value;
         });
      }

      if($data) {
         return response()->json($data, 200);

      } else {
         return response()->json(['_errors' => ['No restaurant available']], 422);
      }
   }

   public function details($restaurantid)
   {
      $data = Restaurant::select('id', 'name', 'cover as image', 'latitude', 'longitude', 
            'address1', 'address2', 'address3', 'postcode', 'city', 'state', 'country')
         ->where('id', $restaurantid)
         ->first();

      if($data) {
         $url = URL::to('/');
         $data->image = ($data->image == null) ? null : $url.$data->image;
         return response()->json($data, 200);

      } else {
         return response()->json(['_errors' => ['Invalid restaurant']], 422);
      }
   }
}
