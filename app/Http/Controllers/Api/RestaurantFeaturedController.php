<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\RestaurantFeatured;
use App\Http\Models\Restaurant;
use URL;

class RestaurantFeaturedController extends Controller
{
   public function index()
   {
   	$featured = RestaurantFeatured::select('restaurant_id')->where('is_active', true)->orderBy('sorting')->get();
   	if($featured) {
         $url = URL::to('/');
         $data = [];
         foreach($featured as $key => $value) {
            $restaurant = Restaurant::select('id', 'name', 'cover', 'latitude', 'longitude', 'address1', 'address2', 'address3', 'postcode', 'city', 'state', 'country')
               ->where('id', $value['restaurant_id'])
               ->first();

            if($restaurant) {
               $data[$key]['id'] = $restaurant['id'];
               $data[$key]['name'] = $restaurant['name'];
               $data[$key]['image'] = ($restaurant['cover'] == null) ? null : $url.$restaurant['cover'];
               $data[$key]['latitude'] = $restaurant['latitude'];
               $data[$key]['longitude'] = $restaurant['longitude'];
               $data[$key]['address1'] = $restaurant['address1'];
               $data[$key]['address2'] = $restaurant['address2'];
               $data[$key]['address3'] = $restaurant['address3'];
               $data[$key]['postcode'] = $restaurant['postcode'];
               $data[$key]['city'] = $restaurant['city'];
               $data[$key]['state'] = $restaurant['state'];
               $data[$key]['country'] = $restaurant['country'];
            }
         }

	   	// $data = $featured->map(function ($value, $key) use ($url) {
	   	// 	$restaurant = Restaurant::select('id', 'name', 'cover', 'latitude', 'longitude', 'address1', 'address2', 'address3', 'postcode', 'city', 'state', 'country')
         //       ->where('id', $value['restaurant_id'])
         //       ->first();
            
         //    unset($value['restaurant_id']);
         //    if($restaurant) {
         //       $value['id'] = $restaurant['id'];
         //       $value['name'] = $restaurant['name'];
         //       $value['image'] = ($restaurant['cover'] == null) ? null : $url.$restaurant['cover'];
         //       $value['latitude'] = $restaurant['latitude'];
         //       $value['longitude'] = $restaurant['longitude'];
         //       $value['address1'] = $restaurant['address1'];
         //       $value['address2'] = $restaurant['address2'];
         //       $value['address3'] = $restaurant['address3'];
         //       $value['postcode'] = $restaurant['postcode'];
         //       $value['city'] = $restaurant['city'];
         //       $value['state'] = $restaurant['state'];
         //       $value['country'] = $restaurant['country'];
   
         //       return $value;
         //    }
	   	// });
   	}

      if($data) {
         return response()->json($data, 200);

      } else {
         return response()->json(['_errors' => ['No featured restaurant available']], 422);
      }
   }
}
