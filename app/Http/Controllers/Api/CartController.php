<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Cart;
use App\Http\Models\Customer;
use App\Http\Models\RestaurantMenu;
use Validator;
use Carbon\Carbon;
use URL;

class CartController extends Controller
{
    public function index(Request $request)
    {
    	$customer_id = Customer::where('api_token', $request->header('Authorization'))->value('id');
    	$cart 		 = Cart::select('menu_item_id', 'qty')->where('customer_id', $customer_id)->get();

    	if($cart) {
			$url = URL::to('/');
    		foreach($cart as $value) {
    			$data = RestaurantMenu::select('id', 'restaurant_id', 'name', 'image', 'price', 'description')->where('id', $value->menu_item_id)->first();

    			if($data) {
					$value->id            = $data->id;
					$value->restaurant_id = $data->restaurant_id;
					$value->name          = $data->name;
					$value->image         = $url.$data->image;
					$value->price         = $data->price;
					$value->description   = $data->description;
					$value->quantity      = $value->qty;
					$value->total         = round(($value->qty * $data->price), 2);
					unset($value->menu_item_id);
					unset($value->qty);
    			}
    		}
    		
   			return response()->json($cart, 200);

   		} else {
   			return response()->json(['_errors' => ['Cart is empty']], 400);

   		}
    }

    public function add(Request $request)
    {
	   	// Validate Rules
	   	$rules = [
	   		'menu_item_id' => 'required|integer',
	   		'qty'          => 'required|integer',
	      ];

	    // Validate Request
	    $validator = Validator::make($request->all(), $rules);

	   	if($validator->fails()) {
	   		return response()->json(['errors' => $validator->errors()], 400);

	   	} else {
	   		$customer_id = Customer::where('api_token', $request->header('Authorization'))->value('id');

	   		$active = RestaurantMenu::where('id', $request->menu_item_id)->where('is_active', true)->exists();
	   		if($active) {
	   			if($request->qty == 0) {
	   				$data = Cart::where('customer_id', $customer_id)->where('menu_item_id', $request->menu_item_id)->first();
	   				if($data) {
				   		// $debug = Cart::where('customer_id', $customer_id)->get();
			   			// return response()->json($debug, 200);
			   			
			   			$data->delete();
			   			$data->save();
			   			return response()->json(['_status' => 'OK'], 200);

			   		} else {
			   			return response()->json(['_errors' => ['Cart is empty']], 400);
			   		}
	   			} else {
			   		$data = Cart::updateOrCreate(
			   			[
								'customer_id'  => $customer_id,
								'menu_item_id' => $request->menu_item_id,
				   		],
				        [
								'qty'          => $request->qty,
				   		]
				   	);


			   		if($data) {
				   		// $debug = Cart::where('customer_id', $customer_id)->get();
			   			// return response()->json($debug, 200);
			   			return response()->json(['_status' => 'OK'], 200);

			   		} else {
			   			return response()->json(['_errors' => ['Fail to add to cart']], 400);
			   		}
	   			}

	   		} else {
	   			return response()->json(['_errors' => ['Item is unavailable']], 400);
	   		}
	   	}
    }

    public function clear(Request $request)
    {
    	$customer_id = Customer::where('api_token', $request->header('Authorization'))->value('id');
    	$data = Cart::where('customer_id', $customer_id)->delete();

   		if($data) {
   			return response()->json(['_status' => 'OK'], 200);

   		} else {
   			return response()->json(['_errors' => ['Fail to add to cart']], 400);
   		}
    }

	// Check menu availbility
	public function checkout(Request $request)
	{
    	$customer_id = Customer::where('api_token', $request->header('Authorization'))->value('id');
    	$cart = Cart::select('menu_item_id', 'qty')->where('customer_id', $customer_id)->get();

    	if($cart) {
    		$check  = RestaurantMenu::checkAvailbility($cart);
    		$getAmount = RestaurantMenu::getAmount($cart);
    	}

		if($check == 1) {
			return response()->json($getAmount, 200);
		} else {
			return response()->json(['_errors' => ['Item is not available']], 422);
		}
	}
}
