<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Banner;
use URL;
use Carbon\Carbon;
use App\Http\Models\RestaurantHoliday;

class BannerController extends Controller
{
   public function index()
   {
   	$url = URL::to('/');
   	$banner = Banner::select('id', 'image')->where('is_active', true)->orderBy('sorting')->get();
   	$data = $banner->map(function ($value, $key) use ($url) {
   	    $value['image'] = $url.$value['image'];
   	    return $value;
   	});

      if($banner) {
         return response()->json($data, 200);

      } else {
         return response()->json(['_errors' => ['No banner available']], 422);
      }
   }
}
