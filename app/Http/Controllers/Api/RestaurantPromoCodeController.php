<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\RestaurantPromoCode;
use App\Http\Models\RestaurantPromoCodeRedeem;
use App\Http\Models\Customer;
use Carbon\Carbon;

class RestaurantPromoCodeController extends Controller
{
    public function index(Request $request)
    {
    	$data = RestaurantPromoCode::select('id', 'value', 'type')->where('code', strtoupper($request->promoCode))->where('is_active', true)->first();
    	$customer_id = Customer::getId($request->header('Authorization'));

    	if($data) {
    		$redeem = RestaurantPromoCodeRedeem::where('user_id', $customer_id)->where('code', $request->promoCode)->exists();

    		if($redeem) {
				return response()->json(['_errors' => ['Code already been redeemed']], 400);
			} else {
				if($data->type == 'percentage') {
					$data->message = $data->value.'% discount applied';
				} elseif($data->type == 'amount') {
					$data->message = 'MYR'.$data->value.' discount applied';
				}
				
				return response()->json($data, 200);
			}

    	} else {
    		return response()->json(['_errors' => ['Invalid code']], 422);
    	}
    }
}
