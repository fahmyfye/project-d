<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Models\Otp;
use Str;
use Hash;
use Validator;
use Auth;
use Mail;

class CustomerController extends Controller
{

   /**
    * Create a new user instance after a valid registration.
    */
   public function register(Request $request) {
   	// Validate Rules
   	$rules = [
         'name'     => 'required|string|max:255',
         'email'    => 'required|email|max:255|unique:customers',
         'password' => 'required|min:4',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

   	if($validator->fails()) {
   		return response()->json(['_errors' => $validator->errors()->all()], 400);

   	} else {
   		$data = Customer::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'device'    => isset($request->device) ? $request->device : null,
            'password'  => Hash::make($request->password),
            'phone'     => isset($request->phone) ? $request->phone : null,
            'latitude'  => isset($request->latitude) ? $request->latitude : null,
            'longitude' => isset($request->longitude) ? $request->longitude : null,
            'city'      => isset($request->city) ? $request->city : null,
            'state'     => isset($request->state) ? $request->state : null,
            'avatar'    => isset($request->avatar) ? $request->avatar : null,
            'api_token' => Str::random(60),
            'social'    => isset($request->social) ? $request->social : 'email',
            'social_id' => isset($request->social_id) ? $request->social_id : null,
   		]);

   		if($data) {
            $auth = Customer::getInfo($data->api_token);

   			return response()->json($auth, 200);

   		} else {
   			return response()->json(['_errors' => ['Fail to register']], 400);

   		}
   	}
   }

   public function login(Request $request) {
   	$data = Customer::where('email', $request->email)->first();

   	if($data) {
   		$hashed = Hash::check($request->password, $data->password);
   		if($hashed) {
   			if($data->api_token == null) {
   				$data->api_token = Str::random(60);
   				$data->save();
   			}

            $auth = Customer::getInfo($data->api_token);

   			return response()->json($auth, 200); 
   			
   		} else {
   			return response()->json(['_errors' => ['Unauthorised']], 401);
   		}

   	} else {
   		return response()->json(['_errors' => ['Unregistered User']], 401);
   	}
   }

   public function logout(Request $request) {
      $data = Customer::where('api_token', $request->header('Authorization'))->first();
      $data->api_token = null;
      $data->save();

      if($data) {
         return response()->json([
            '_status' => 'OK', 
            '_message' => 'Logged out successfully'
         ], 200);

      } else {
         return response()->json(['_errors' => ['Fail to logout']], 422);

      }
   }

   // Get Customer Details
   public function info(Request $request) {
      $data = Customer::getInfo($request->header('Authorization'));

      if($data) {
         return response()->json($data, 200);

      } else {
         return response()->json(['_errors' => ['User does not exist']], 404);

      }
   }

   // Update Customer Details
   public function update(Request $request) {
      // Validate Rules
      $rules = [
         'name' => 'required|string|max:255',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()) {
         return response()->json(['_errors' => $validator->errors()->all()], 422);

      } else {
         $data = Customer::where('api_token', $request->header('Authorization'))->update([
            'name'      => $request->name,
            'phone'     => isset($request->phone) ? $request->phone : null,
            'latitude'  => isset($request->latitude) ? $request->latitude : null,
            'longitude' => isset($request->longitude) ? $request->longitude : null,
            'city'      => isset($request->city) ? $request->city : null,
            'state'     => isset($request->state) ? $request->state : null,
            'avatar'    => isset($request->avatar) ? $request->avatar : null,
         ]);

         if($data) {
            $auth  = Customer::getInfo($request->header('Authorization'));
            return response()->json($auth, 200);

         } else {
            return response()->json(['_errors' => ['Fail to update user info']], 422);

         }
      }
   }

   // Update Customer Token
   public function updateToken(Request $request)
   {
      // Validate Rules
      $rules = [
         'email'          => 'required|email|max:255',
         'device'         => 'required|string|max:255',
         'firebase_token' => 'required|string|max:255',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()) {
         return response()->json(['_errors' => $validator->errors()->all()], 400);

      } else {
         $data = Customer::where('email', $request->email)->first();
         if($data) {
            $data->device         = $request->device;
            $data->firebase_token = $request->firebase_token;
            $data->save();

            if($data) {
               return response()->json([
                  '_status' => 'OK',
                  '_message' => 'Device token has been updated'
               ], 200);

            } else {
               return response()->json(['_errors' => ['Server error']], 422);
            }

         } else {
            return response()->json(['_errors' => ['Invalid user email']], 422);

         }
      }
      
   }

   // Chage password
   protected function password(Request $request)
   {
      // Validate Rules
      $rules = [
         'oldPassword' => 'required',
         'newPassword' => 'required|min:4',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()) {
         return response()->json(['_errors' => $validator->errors()->all()], 400);

      } else {
         $data = Customer::where('api_token', $request->header('Authorization'))->first();

         if($data) {
            $check = Hash::check($request->oldPassword, $data->password);

            if($check) {
               $data->password = Hash::make($request->newPassword);
               $data->save();

               if($data) {
                  $auth = Customer::getInfo($request->header('Authorization'));

                  return response()->json(['_status' => 'OK', '_message' => 'Your password has been changed'], 200);

               } else {
                  return response()->json(['_errors' => ['Fail to update password']], 422);

               }

            } else {
               return response()->json(['_errors' => ['Your current password is invalid']], 422);

            }

         } else {
            return response()->json(['_errors' => ['Invalid user']], 422);
         }
      }
   }

   // Forgot Password
   protected function forgot(Request $request)
   {
      // Validate Rules
      $rules = [
         'email' => 'required|email',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()) {
         return response()->json(['_errors' => $validator->errors()->all()], 400);

      } else {
         $customer_id = Customer::where('email', $request->email)->value('id');

         if($customer_id) {
            $to_name  = $request->email;
            $to_email = $request->email;
            $otp      = Otp::generate();

            $data = 
            [
               'title' => 'Your OTP Request for Forgot Password',
               'body' => 'Your OTP Number: '.$otp
            ];

            $mail = Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
               $message->to($to_email, $to_name)->subject('Dsaji - Forgot Password');
               $message->from(config('mail.username', null), 'DSaji Admin');
            });

            $createOtp = Otp::create([
               'email'       => $request->email,
               'module'      => 'forgotPassword',
               'otp'         => $otp,
            ]);

            return response()->json([
               '_status' => 'OK',
               '_message' => 'A mail has been sent with otp, Please check your mail'
            ], 200);

         } else {
            return response()->json(['_errors' => ['Invalid email']], 402);
         }
      }
   }

   protected function reset(Request $request)
   {
      // Validate Rules
      $rules = [
         'email'       => 'required|email',
         'newPassword' => 'required|min:4',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()) {
         return response()->json(['_errors' => $validator->errors()->all()], 400);

      } else {
         $data = Customer::where('email', $request->email)->first();

         if($data) {
            $data->password = Hash::make($request->newPassword);
            $data->save();

            if($data) {
               $auth = Customer::getInfo($data->api_token);

               return response()->json(['_status' => 'OK', '_message' => 'Your password has been changed'], 200);

            } else {
               return response()->json(['_errors' => ['Fail to update password']], 422);

            }

         } else {
            return response()->json(['_errors' => ['Invalid email']], 422);
         }
         
      }
   }

   // Verify OTP
   public function otp(Request $request)
   {
      // Validate Rules
      $rules = [
         'email' => 'required|email',
         'otp'   => 'required|digits:5',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()) {
         return response()->json(['_errors' => $validator->errors()->all()], 400);

      } else {
         $otp = Otp::where('email', $request->email)->where('otp', $request->otp)->exists();

         if($otp) {
            return response()->json(['_status' => 'OK'], 200);

         } else {
            return response()->json(['_errors' => ['Invalid email / otp']], 422);
         }
      }
   }
}
