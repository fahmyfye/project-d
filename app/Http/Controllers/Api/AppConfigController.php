<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\AppService;
use App\Http\Models\AppVersion;

class AppConfigController extends Controller
{
    public function index(Request $request)
    {
    	$service = AppService::select('scanqr', 'home_delivery')->where('device', $request->device)->first();
    	$version = AppVersion::select('current_version', 'force_update', 'message')->where('device', $request->device)->first();

    	if($service && $version) {
            return response()->json([
                'available_service' => $service,
                'app_version'       => $version
            ], 200);
    	} else {
    		return response()->json(['_errors' => ['Server Error']], 400);
    	}
    }

    public function updateVersion(Request $request) {
        $update = AppVersion::select('current_version', 'force_update', 'message')->where('device', $request->device)->update([
            'current_version' => $request->version,
        ]);

        if($update) {
            $service = AppService::select('scanqr', 'home_delivery')->where('device', $request->device)->first();
    	    $version = AppVersion::select('current_version', 'force_update', 'message')->where('device', $request->device)->first();
            return response()->json([
                'available_service' => $service,
                'app_version'       => $version
            ], 200);
    	} else {
    		return response()->json(['_errors' => ['Server Error']], 400);
    	}
        
    }
}

