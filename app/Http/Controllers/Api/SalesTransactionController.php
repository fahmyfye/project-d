<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\RestaurantMenu;
use App\Http\Models\RestaurantSalesOrder;
use App\Http\Models\RestaurantSalesDetails;
use App\Http\Models\Customer;
use App\Http\Models\CustomerAddress;
use App\Http\Models\RestaurantPromoCode;
use App\Http\Models\Cart;
use App\Http\Models\SalesTransaction;
use App\Http\Models\eGHL;
use Validator;
use Str;
use Carbon\Carbon;

class SalesTransactionController extends Controller
{
    public function create(Request $request)
    {
        // Validate Rules
        $rules = [
            'reference_no' => 'required|string|max:25',
            'amount'       => 'required',
            'gateway'      => 'required|string|max:255',
            'payment_type' => 'required|string|max:255',
        ];

        // Validate Request
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['_errors' => $validator->errors()], 400);
        } else {
            $customer_id = Customer::where('api_token', $request->header('Authorization'))->value('id');
            $payment_id   = Carbon::now()->format('His') . strtoupper(Str::random(4));

            // Save in SalesOrder
            $trx = SalesTransaction::create([
                'payment_id'   => $payment_id,
                'reference_no' => $request->reference_no,
                'customer_id'  => $customer_id,
                'customer_ip'  => $request->ip(),
                'amount'       => $request->amount,
                'gateway'      => $request->gateway,
                'payment_type' => $request->payment_type,
            ]);

            if ($trx) {
                // $trx->customer = Customer::select('name', 'email', 'phone')->where('id', $trx->customer_id)->first();
                // if ($request->gateway == 'eghl') {
                //     $pay = eGHL::paymentRequest($trx);

                //     if ($pay == 0) {
                //         return response()->json(['_errors' => ['Fail to create transaction']], 422);
                //     }
                // } elseif ($request->gateway == 'billplz') {
                //     $pay = eGHL::paymentRequest($trx);

                //     if ($pay == 0) {
                //         return response()->json(['_errors' => ['Fail to create transaction']], 422);
                //     }
                // }

                // if ($pay == 1) {
                //     $data = SalesTransaction::select('payment_id', 'reference_no', 'amount', 'gateway', 'payment_type', 'status')
                //         ->where('payment_id', $trx->payment_id)
                //         ->where('reference_no', $trx->reference_no)
                //         ->first();

                //     $data->status = 'completed';
                //     $data->save();

                //     // Clear cart on success
                //     if ($data) {
                //         $cart = Cart::where('customer_id', $trx->customer_id)->delete();
                //     }
                // }
                return response()->json($trx, 200);
            } else {
                return response()->json(['_errors' => ['Fail to create transaction']], 422);
            }
        }
    }

    public function update(Request $request) {
        $trx = SalesTransaction::where('reference_no', $request->reference_no)->first();
        if($trx) {
            if(isset($request->transaction_status)) {
                $trx->status = $request->transaction_status;
                $trx->save();
            }
        }

        $sales = RestaurantSalesOrder::where('reference_no', $request->reference_no)->first();
        if($sales) {
            if(isset($request->order_status)) {
                $sales->order_status = $request->order_status;
            }

            if(isset($request->delivery_status)) {
                $sales->delivery_status = $request->delivery_status;
            }
            
            $sales->save();

            $data = RestaurantSalesOrder::where('reference_no', $request->reference_no)->first();
        }
        
        if(isset($data) && !empty($data)) {
            return response()->json($data, 200);
        } else {
            return response()->json(['_errors' => ['Fail to update transaction']], 422);
        }
    }
}
