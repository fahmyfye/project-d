<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Models\RestaurantHoliday;

class RestaurantHolidayCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restaurant:holiday-start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks for holiday set by restaurant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $holiday = RestaurantHoliday::cronStart();
    }
}
