<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<!-- BEGIN: Head-->
<head>
   @include('layouts.partials.header')
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu 2-column fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

   <!-- BEGIN: Header-->
   @include('layouts.partials.toppanel')
   <!-- END: Header-->

   <!-- BEGIN: Content-->
   @yield('content')
   <!-- END: Content-->

   <!-- BEGIN: Main Menu-->
   @include('layouts.partials.menu')
   <!-- END: Main Menu-->

   <div class="sidenav-overlay"></div>
   <div class="drag-target"></div>

   <!-- BEGIN: Footer-->
   <footer class="footer footer-static footer-dark navbar-border">
      @include('layouts.partials.footer')       
   </footer>
   <!-- END: Footer-->

   <!-- BEGIN: Vendor JS-->
   <script src="{{ asset('vendors/js/vendors.min.js') }}"></script>
   <!-- BEGIN Vendor JS-->

   <!-- BEGIN: Page Vendor JS-->
   <script src="{{ asset('vendors/js/ui/prism.min.js') }}"></script>
   <script src="{{ asset('vendors/js/selects/select2.full.min.js') }}"></script>
   <script src="{{ asset('vendors/js/pickers/datetime/moment-with-locales.min.js') }}"></script>
   <script src="{{ asset('vendors/js/pickers/datetime/bootstrap-datetimepicker.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/datatables/datatables.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/datatables/dataTables.buttons.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tables/datatables/buttons.bootstrap4.min.js') }}"></script>
   <script src="{{ asset('vendors/js/tags/tagging.min.js') }}"></script>
   <script src="{{ asset('vendors/js/repeater/jquery.repeater.min.js') }}"></script>
   @yield('vendorjs')
   <!-- END: Page Vendor JS-->

   <!-- BEGIN: Theme JS-->
   <script src="{{ asset('js/core/app-menu.min.js') }}"></script>
   <script src="{{ asset('js/core/app.min.js') }}"></script>
   {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}
   <!-- END: Theme JS-->

   <!-- BEGIN: Page JS-->
   <script src="{{ asset('js/scripts/bootstrap-toggle.js') }}"></script>
   <script src="{{ asset('js/scripts/custom-file-input.js') }}"></script>
   @yield('js')
   <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
