@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Restaurant Category</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">Settings</li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.category', $restaurantid) }}">Category</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">Category</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <a href="{{ route('restaurants.category.create', $restaurantid ) }}" class="btn btn-primary">
                      <i class="ft-plus"></i> New Category
                  </a>
               </div>
            </div>

            <div class="card-content">
               <div class="card-body">
                  <h4 class="card-title"></h4>

                  @if(!empty($data))
                     <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th class="text-center" width="1%">#</th>
                                 <th>Name</th>
                                 <th class="text-center" width="25%">Image</th>
                                 <th class="text-center" width="10%">Sorting</th>
                                 <th class="text-center" width="5%">Status</th>
                                 <th class="text-center" width="10%"></th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($data as $key => $value)
                              <tr>
                                 <td class="text-center">{{ ($key+1) }}</td>
                                 <td>{{ $value->name }}</td>
                                 <td class="text-center">
                                    @empty($value->image)
                                       <div class="alert alert-warning">No Image</div>
                                    @else
                                       @if(strpos($value->image, 'http') === true)
                                          <div class="media-right" href="#">
                                             <img class="border border-1 rounded border-dark" src="{{ $value->image }}" style="max-width: 250px;" />
                                          </div>
                                       @else
                                          <div class="media-right" href="#">
                                             <img class="border border-1 rounded border-dark" src="{{ asset($value->image) }}" style="max-width: 250px;" />
                                          </div>
                                       @endif
                                    @endif
                                 </td>
                                 <td class="text-center">{{ $value->sorting }}</td>
                                 <td class="text-center">
                                    @if($value->is_active == 1)
                                       <div class="badge badge-success round">
                                          <span>Active</span>
                                          <i class="ft-check font-medium-2"></i>
                                       </div>
                                    @else
                                       <div class="badge badge-danger round">
                                          <span>In-Active</span>
                                          <i class="ft-x font-medium-2"></i>
                                       </div> 
                                    @endif
                                 </td>
                                 <td class="text-center">
                                    <div class="form-group">
                                       <div class="btn-group" role="group">
                                          <!-- Button group with icons -->
                                          <a href="{{ route('restaurants.category.edit', ['restaurantid' => $restaurantid, 'id' => $value->id]) }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Category"><i class="ft-edit-3"></i></a>

                                          <form id="deleteform_{{ $value->id }}" method="post" action="{{ route('restaurants.category.delete', $restaurantid) }}">
                                             @method('delete')
                                             @csrf

                                             <input type="hidden" name="id" value="{!! $value->id !!}">
                                             <button id="delete_{{ $value->id }}" type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Category"><i class="ft-x"></i></button>
                                          </form>

                                       </div>
                                    </div>
                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  @else
                     <div class="alert alert-warning" role="alert">No categories set</div>
                  @endif
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('#table').DataTable({
         'scrollX'     : true,
         'paging'      : true,
         'lengthChange': true,
         'searching'   : true,
         'ordering'    : true,
         'info'        : true,
      })
   })
   
   $('[id^=delete_]').click(function() {
      if (confirm('Delete this category?')) {
         x = this.id
         y = x.replace('delete_', 'deleteform_')

         $('#'+y).submit()
      }
   })
</script>
@endsection
