@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Dashboard</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         @if (isset($data))
            <section class="card">
               <div class="card-header">
                  <h4 class="card-title"></h4>
               </div>
               <div class="card-content">
                  <div class="card-body">
                     <div class="card-text">
                        <div class="table-responsive">
                           <table id="table" class="table table-hover table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th class="text-center" width="1%">#</th>
                                    <th>Name</th>
                                    <th class="text-center" width="20%">Class</th>
                                    <th class="text-center" width="15%">Emergency Contact</th>
                                    <th class="text-center" width="5%">Status</th>
                                    <th width="5%"></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @foreach($data as $key => $value)
                                    <tr>
                                       <td class="text-center">{!! ($key+1) !!}</td>
                                       <td>{!! $value->name !!}</td>
                                       <td class="text-center">{!! $value->classroom->name !!}</td>
                                       <td class="text-center">
                                          <ul class="list-style-square">
                                             <li>{!! $value->emergency_name !!}</li>
                                             <li>{!! $value->emergency_phone !!}</li>
                                             <li>{!! $value->emergency_relationship !!}</li>
                                          </ul>
                                       </td>
                                       <td class="text-center">{!! $value->status->name !!}</td>
                                       <td class="text-center">
                                          <div class="btn-group" role="group" aria-label="Basic example">
                                             <a href="{{ route('children.view', $value->id) }}" class="btn btn-info"><i class="ft-search" data-toggle="tooltip" data-placement="top" title="View Details"></i></a>
                                          </div>
                                       </td>
                                    </tr>
                                 @endforeach
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </section>

         @else
            <div class="alert alert-warning">Blank Dashboard</div>
            
         @endif
      </div>
   </div>
</div>
@endsection