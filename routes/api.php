<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/'], function() {
	// App
	Route::group(['prefix' => 'app'], function() {
		Route::post('/config', 'Api\AppConfigController@index');
		Route::post('/config/version', 'Api\AppConfigController@updateVersion');
		Route::get('/banner', 'Api\BannerController@index');
		Route::group(['prefix' => 'restaurant'], function() {
			Route::get('/', 'Api\RestaurantController@index');
			Route::get('/details/{restaurantid}', 'Api\RestaurantController@details');
			Route::get('/featured', 'Api\RestaurantFeaturedController@index');
			Route::get('/menu/{restaurantid}', 'Api\RestaurantMenuController@index');
			Route::get('/menu/{restaurantid}/item/{categoryid}', 'Api\RestaurantMenuController@item');
			Route::get('/menu/category/{restaurantid}', 'Api\CategoryController@index');
		});
		Route::group(['prefix' => 'general'], function() {
			Route::get('/privacy', 'Api\GeneralController@privacy');
            Route::get('/tnc', 'Api\GeneralController@tnc');
		    Route::get('/state', 'Api\GeneralController@stateIndex');
		});
	});

	// User Module
	Route::group(['prefix' => 'user'], function() {
		Route::post('/', 'Api\CustomerController@login');
		Route::get('/', 'Api\CustomerController@info');
		Route::post('/update', 'Api\CustomerController@update');
		Route::post('/register', 'Api\CustomerController@register');
		Route::post('/logout', 'Api\CustomerController@logout');
		Route::post('/password', 'Api\CustomerController@password');
		Route::post('/forgot', 'Api\CustomerController@forgot');
		Route::post('/reset', 'Api\CustomerController@reset');
		Route::post('/otp', 'Api\CustomerController@otp');

		// User Address Module
		Route::group(['prefix' => 'address'], function() {
			Route::post('/', 'Api\CustomerAddressController@create');
			Route::post('/addresses', 'Api\CustomerAddressController@addressList');
			Route::post('/delete', 'Api\CustomerAddressController@delete');
			Route::get('/state', 'Api\CustomerAddressController@state');
		});

		// Token
		Route::post('/updateToken', 'Api\CustomerController@updateToken');
	});

	Route::group(['prefix' => 'cart'], function() {
		Route::post('/', 'Api\CartController@index');
		Route::post('/add', 'Api\CartController@add');
		Route::post('/clear', 'Api\CartController@clear');
		Route::post('/checkout', 'Api\CartController@checkout');
	});

	Route::group(['prefix' => 'sales'], function() {
		Route::post('/', 'Api\SalesController@index');
		Route::post('/details', 'Api\SalesController@details');
		Route::post('/create', 'Api\SalesController@create');
	});

	Route::group(['prefix' => 'transaction'], function() {
		Route::post('/', 'Api\SalesTransactionController@index');
        Route::post('/create', 'Api\SalesTransactionController@create');
        Route::post('/update', 'Api\SalesTransactionController@update');

        Route::group(['prefix' => 'eghl'], function() {
            Route::get('/callback/{eghl}', 'Api\SalesTransactionController@eghlCallback');
        });

        Route::group(['prefix' => 'billplz'], function() {
            Route::post('/create', 'Api\BillPlzController@create');
            Route::post('/callback', 'Api\BillPlzController@callback');
            Route::get('/query/{bill_id}', 'Api\BillPlzController@query');
        });
	});

	Route::group(['prefix' => 'promo'], function() {
		Route::post('/', 'Api\RestaurantPromoCodeController@index');
	});

	Route::group(['prefix' => 'table'], function() {
		Route::get('/{restaurantid}/{tableno}', 'Api\RestaurantTableController@index');
	});
});
