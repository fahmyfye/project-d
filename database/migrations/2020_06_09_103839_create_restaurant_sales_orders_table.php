<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantSalesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_sales_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_no')->unique();
            $table->string('restaurant_id');
            $table->string('type');
            $table->integer('address_id')->nullable();
            $table->integer('customer_id');
            $table->string('email');
            $table->decimal('amount', 8, 2);
            $table->string('discount_code')->nullable();
            $table->decimal('discount_amount', 8, 2)->default(0);
            $table->decimal('delivery_amount', 8 ,2)->default(0);
            $table->string('total_amount');
            $table->string('order_status')->default('pending');
            $table->string('delivery_status')->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_sales_orders');
    }
}
