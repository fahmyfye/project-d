<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payment_id')->unique();
            $table->string('reference_no');
            $table->integer('customer_id');
            $table->string('customer_ip')->nullable();
            $table->string('amount');
            $table->string('currency')->default('MYR');
            $table->string('gateway');
            $table->string('payment_type');
            $table->text('raw_json')->nullable();
            $table->string('status')->default('pending');
            $table->boolean('initialized')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_transactions');
    }
}
