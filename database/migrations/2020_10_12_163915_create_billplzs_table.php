<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillplzsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billplzs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payment_id')->unique();
            $table->string('reference_no');
            $table->string('bill_id');
            $table->string('reference_1')->nullable();
            $table->string('reference_2')->nullable();
            $table->boolean('paid')->default(0);
            $table->text('json_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billplzs');
    }
}
