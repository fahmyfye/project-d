<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantSalesTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_sales_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_no');
            $table->string('user_id');
            $table->string('payment_id')->unique();
            $table->decimal('amount', 8, 2);
            $table->string('currency')->nullable();
            $table->string('exchange_rate')->nullable();
            $table->string('payment_gateway');
            $table->string('payment_type');
            $table->string('raw_json');
            $table->string('status');
            $table->tinyInteger('completed')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_sales_transactions');
    }
}
