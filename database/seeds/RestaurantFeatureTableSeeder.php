<?php

use Illuminate\Database\Seeder;

class RestaurantFeatureTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_featureds')->insert([
      	[
				'restaurant_id' => 1,
				'feature_start' => '2020-06-26 00:00:00',
				'feature_end'   => '2020-12-31 00:00:00',
				'sorting'       => 1,
				'is_active'     => 1,
				'created_at'    => new DateTime(),
	      ],
      	[
				'restaurant_id' => 2,
				'feature_start' => '2020-06-26 00:00:00',
				'feature_end'   => '2020-12-31 00:00:00',
				'sorting'       => 10,
				'is_active'     => 1,
				'created_at'    => new DateTime(),
	      ],
      	[
				'restaurant_id' => 3,
				'feature_start' => '2020-06-26 00:00:00',
				'feature_end'   => '2020-12-31 00:00:00',
				'sorting'       => 2,
				'is_active'     => 1,
				'created_at'    => new DateTime(),
	      ],
      	[
				'restaurant_id' => 4,
				'feature_start' => '2020-06-26 00:00:00',
				'feature_end'   => '2020-12-31 00:00:00',
				'sorting'       => 99,
				'is_active'     => 1,
				'created_at'    => new DateTime(),
	      ],
	   ]);
   }
}
