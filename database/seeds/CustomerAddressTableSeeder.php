<?php

use Illuminate\Database\Seeder;

class CustomerAddressTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('customer_addresses')->insert([
      	[
				'customer_id' => 1,
				'name'        => 'Home',
				'phone'       => '+60102345588',
				'address1'    => 'No.6 Jalan 4/5',
				'address2'    => 'Ampang Hilir',
				'postcode'    => '55100',
				'city'        => 'Ampang',
				'state'       => 'Selangor',
				'country'     => 'Malaysia',
				'default'     => true,
				'active'      => true,
				'created_at'  => new DateTime(),
	      ]
	   ]);
   }
}