<?php

use Illuminate\Database\Seeder;

class AppVersionTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('app_versions')->insert([
      	[
				'device'          => 'android',
				'current_version' => '1.2.0',
				'force_update'    => 'yes',
				'message'         => 'This version fixes some bugs and improves performance',
				'created_at'      => new DateTime(),
	      ],
         [
            'device'          => 'ios',
            'current_version' => '1.2.0',
            'force_update'    => 'yes',
            'message'         => 'This version fixes some bugs and improves performance',
            'created_at'      => new DateTime(),
         ]
	   ]);
   }
}
