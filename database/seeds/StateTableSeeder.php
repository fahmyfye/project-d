<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
	   	DB::table('states')->insert([
	   		[
	   			'name'       => 'Kuala Lumpur',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Labuan',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Putrajaya',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Johor',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Kedah',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Kelantan',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Melaka',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Negeri Sembilan',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Pahang',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Perak',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Perlis',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Penang',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Sabah',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Serawak',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Selangor',
	   			'created_at' => new DateTime(),
	   		],
	   		[
	   			'name'       => 'Terengganu',
	   			'created_at' => new DateTime(),
	   		],
	   	]);
    }
}
