<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantHolidayTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_holidays')->insert([        
         [
				'restaurant_id' => 1,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_date'      => '2020-05-01',
				'is_closed'		 => 0,
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 1,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_date'      => '2020-05-24',
				'is_closed'		 => 1,
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 1,
				'ops_start'     => null,
				'ops_end'       => null,
				'ops_date'      => '2020-05-25',
				'is_closed'		 => 1,
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 3,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_date'      => '2020-05-01',
				'is_closed'		 => 0,
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 4,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_date'      => '2020-05-01',
				'is_closed'		 => 0,
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 5,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_date'      => '2020-05-01',
				'is_closed'		 => 0,
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 6,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_date'      => '2020-05-01',
				'is_closed'		 => 0,
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 7,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_date'      => '2020-05-01',
				'is_closed'		 => 0,
				'created_at'    => Carbon::now(),
         ],
      ]);
   }
}
