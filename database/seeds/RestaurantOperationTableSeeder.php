<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantOperationTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_operations')->insert([        
         [
				'restaurant_id' => 1,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_day'       => '["Tuesday", "Wednesday", "Friday"]',
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 1,
				'ops_start'     => '9:00:00',
				'ops_end'       => '18:00:00',
				'ops_day'       => '["Monday", "Saturday"]',
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 3,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_day'       => '["Tuesday", "Wednesday", "Friday"]',
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 4,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_day'       => '["Tuesday", "Wednesday", "Friday"]',
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 5,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_day'       => '["Tuesday", "Wednesday", "Friday"]',
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 6,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_day'       => '["Tuesday", "Wednesday", "Friday"]',
				'created_at'    => Carbon::now(),
         ],
         [
				'restaurant_id' => 7,
				'ops_start'     => '8:20:00',
				'ops_end'       => '18:30:00',
				'ops_day'       => '["Tuesday", "Wednesday", "Friday"]',
				'created_at'    => Carbon::now(),
         ],
      ]);
   }
}
