<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CustomerTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('customers')->insert([
      	[
				'name'       => 'Fahmy',
				'email'      => 'fahmyapi@fye.com',
				'phone'      => '+60102345678',
				'password'   => bcrypt('123456'),
				'city'       => 'Cyberjaya',
				'state'      => 'Selangor',
				'api_token'	 => null,
				'created_at' => Carbon::now(),
	      ],
	      [
				'name'       => 'DevCustomer',
				'email'      => 'devapi@fye.com',
				'phone'      => '+60102345678',
				'password'   => bcrypt('123456'),
				'city'       => 'Cyberjaya',
				'state'      => 'Selangor',
				'api_token'	 => 'UH2H9Ul46d5ujCDqqywpzQZ8MRiGeJb6K3y0Or21bgK8SRMeJskKAozRqLix',
				'created_at' => Carbon::now(),
	      ],
	   ]);
   }
}
