<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantIngredientTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_ingredients')->insert([
      	[
				'restaurant_id' => 1,
				'name'          => 'Pastry - Butterscotch Baked',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Butter - Unsalted',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Cabbage Roll',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Scallops - 20/30',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Rice - Aborio',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Beef - Tongue, Fresh',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Cheese - Sheep Milk',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Yogurt - Assorted Pack',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'White Fish - Filets',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Cheese - Havarti, Roasted Garlic',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Lentils - Green, Dry',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Celery',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Cheese - Brick With Onion',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Pasta - Detalini, White, Fresh',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Basil - Pesto Sauce',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Salmon - Atlantic, Fresh, Whole',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Beef - Ground Lean Fresh',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Cheese - Woolwich Goat, Log',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Chicken - Whole Fryers',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Chinese Foods - Thick Noodles',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
	   ]);
   }
}
