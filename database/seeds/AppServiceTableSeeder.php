<?php

use Illuminate\Database\Seeder;

class AppServiceTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('app_services')->insert([
      	[
				'device'        => 'android',
				'scanqr'        => 'active',
				'home_delivery' => 'inactive',
				'created_at'    => new DateTime(),
	      ],
         [
            'device'        => 'ios',
            'scanqr'        => 'active',
            'home_delivery' => 'inactive',
            'created_at'    => new DateTime(),
         ]
	   ]);
   }
}
