<?php

use Illuminate\Database\Seeder;

class RestaurantOrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('restaurant_order_statuses')->insert([
    		[
    			'name'        => 'Pending',
    			'slug'        => 'pending',
    			'created_at'  => new DateTime(),
    		],
    		[
    			'name'        => 'Processing',
    			'slug'        => 'processing',
    			'created_at'  => new DateTime(),
    		],
    		[
    			'name'        => 'Completed',
    			'slug'        => 'completed',
    			'created_at'  => new DateTime(),
    		],
    		[
    			'name'        => 'Cancel',
    			'slug'        => 'cancel',
    			'created_at'  => new DateTime(),
    		],
    		[
    			'name'        => 'Refund',
    			'slug'        => 'refund',
    			'created_at'  => new DateTime(),
    		],
    	]);
    }
}
