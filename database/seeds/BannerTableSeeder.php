<?php

use Illuminate\Database\Seeder;

class BannerTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('banners')->insert([
      	[
				'name'       => 'banner1',
				'image'      => '/storage/images/banner/banner1.png',
				'sorting'    => '1',
				'is_active'  => true,
				'created_at' => new DateTime(),
	      ],
      	[
				'name'       => 'banner2',
				'image'      => '/storage/images/banner/banner2.png',
				'sorting'    => '2',
				'is_active'  => true,
				'created_at' => new DateTime(),
	      ],
      	[
				'name'       => 'banner3',
				'image'      => '/storage/images/banner/banner3.png',
				'sorting'    => '3',
				'is_active'  => true,
				'created_at' => new DateTime(),
	      ],
      	[
				'name'       => 'banner4',
				'image'      => '/storage/images/banner/banner4.png',
				'sorting'    => '99',
				'is_active'  => true,
				'created_at' => new DateTime(),
	      ],
	   ]);
   }
}
