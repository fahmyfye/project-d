<?php

use Illuminate\Database\Seeder;

class RestaurantDeliveryTypeTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_delivery_types')->insert([
      	[
				'name'       => 'Home Delivery',
				'slug'       => 'homeDelivery',
				'is_active'  => true,
				'created_at' => new DateTime(),
	      ],
      	[
				'name'       => 'Dine-In',
				'slug'       => 'dineIn',
				'is_active'  => true,
				'created_at' => new DateTime(),
	      ],
      	[
				'name'       => 'Take-Away',
				'slug'       => 'takeAway',
				'is_active'  => true,
				'created_at' => new DateTime(),
	      ],
	   ]);
   }
}
