<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantPromoCodeTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_promo_codes')->insert([
      	[
				'restaurant_id' => 1,
				'name'          => 'Free MYR10.00',
				'code'          => 'FREE10',
				'type'          => 'amount',
				'value'         => 10,
				'allocation'    => 0,
				'is_limited'    => 0,
				'created_by'    => 4,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Limited to 100 Redemption 10% Discount',
				'code'          => 'LIMIT10',
				'type'          => 'percentage',
				'value'         => 10,
				'allocation'    => 100,
				'is_limited'    => 1,
				'created_by'    => 4,
				'created_at'    => Carbon::now(),
	      ],
	   ]);
   }
}
